Starting services of Confluent:

![](screenshots/1.start_confluent.png)

Opening **Control Center** (localhost:9021) and creating topic `expedia` with 3 `partitions`:

![](screenshots/2.create_topic.png)

Showing **created topics**:

![](screenshots/3.created_topics.png)

Showing **3 empty** topic partitions:

![](screenshots/4.topic_partitions.png)

Starting connector with `azblobstorage-source.properties` configuration:

![](screenshots/5.start_connector.png)

Showing the **running connector**:

![](screenshots/6.running_connector.png)

Receiving **messages**:

![](screenshots/7.receiving_messages.png)

Showing **one message**:

![](screenshots/8.showing_message.png)

Showing **3 filled** topic partitions:

![](screenshots/9.filled_partitions.png)

Result of `date_time` field masking:

![](screenshots/10.mask_date_field.png)
